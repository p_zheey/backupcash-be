const location = require('../controllers/location');
const restaurant = require('../controllers/restaurant');
const restaurant_location= require('../controllers/restaurant_location');
const menu_list= require('../controllers/menu_list');

module.exports = {
    location,
    restaurant,
    restaurant_location,
    menu_list

};