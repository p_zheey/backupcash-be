'use strict';
const _ = require('underscore')
const currencyformatterJs = require('currencyformatter.js')
const rp = require('request-promise');
const moment = require('moment');
const stringSimilarity = require('string-similarity');

 const BASE_URL = "https://backupcash-be.atp-sevas.com/"
//const BASE_URL = "http://backupcash.atp-sevas.com/"


const CREATE = (req, res) => {
    const options = {
        method: 'POST',
        uri: BASE_URL+'api/v1/bot/user',
        body: {
            name: req.body.user_first_name,
            withdrawal_pin: req.body.withdrawal_pin,
            facebook_messenger_id: req.body.messenger_id,
            phone: req.body.user_phone_number,
            email: req.body.user_email,
            last_name: req.body.user_last_name,
            source: "facebook"
        },
        json: true
    };

    rp(options)
        .then(function (parsedBody) {
            /*let nextNum = (parsedBody.length - (Number(currentNum) + 10) < 1 ? 0 : (Number(currentNum) + 10))*/

           let response = JSON.parse(JSON.stringify(parsedBody))
        //    let responseData = {
        //        "set_attributes":
        //            {
        //                "referral_link": response.user.referral_link,
        //                "referral_code": response.user.referral_code,
        //            },
        //        "messages": [
        //            {
        //                "text":  "Your account was successfully created. What would you like to do?",
        //                "quick_replies": [
        //                    {
        //                        "title":"Withdraw Money",
        //                        "block_names": ["Verify User"]
        //                    },
        //                    {
        //                        "title":"Quick Deposit",
        //                        "block_names": ["Initiate deposit"]
        //                    }
        //                ]

        //            }
        //        ]
        //    }
            res.status(201).send(parsedBody)
        }).catch(function (err) {
            return  res.status(201).send(err)
        // let responseData;
        //     if(JSON.parse(JSON.stringify(err.error.errors)).hasOwnProperty("email")){
        //         responseData = {
        //             "messages": [
        //                 {
        //                     "text":  JSON.parse(JSON.stringify(err.error.errors)).email[0],
        //                 }
        //             ],
        //             "redirect_to_blocks": ["Enter correct email"]
        //         }
        //       return  res.status(200).send(responseData)
        //     }else if(JSON.parse(JSON.stringify(err.error.errors)).hasOwnProperty("facebook_messenger_id")){
        //         responseData = {
        //             "messages": [
        //                 {
        //                     "text":  "You are already a registered user. What would you like to do?",
        //                     "quick_replies": [
        //                         {
        //                             "title":"Withdraw Money",
        //                             "block_names": ["Verify User"]
        //                         },
        //                         {
        //                             "title":"Quick Deposit",
        //                             "block_names": ["Get email"]
        //                         }
        //                     ]

        //                 }
        //             ]
        //         }
        //         return  res.status(200).send(responseData)
        //     }else if(JSON.parse(JSON.stringify(err.error.errors)).hasOwnProperty("phone")){
        //         responseData = {
        //             "messages": [
        //                 {
        //                     "text":  JSON.parse(JSON.stringify(err.error.errors)).phone[0]
        //                 }
        //             ],
        //             "redirect_to_blocks": ["Enter correct phone"]
        //         }
        //         return  res.status(200).send(responseData)
        //     }else {
        //         responseData = {
        //             "messages": [
        //                 {
        //                     "text":  "I'm currently unable to setup your account. Please, try again later.",

        //                 }
        //             ]
        //         }
        //         return  res.status(200).send(responseData)
        //     }
        });
}

const DEPOSIT = (req, res) => {
    const options = {
        method: 'POST',
        uri: BASE_URL+'api/v1/bot/user/transaction/init',
        body: {
            amount: req.body.amount,
            source: "quick",
            phone: req.body.user_phone_number,
        },
        json: true
    };
    let name = req.body.first_name

    rp(options)
        .then(function (parsedBody) {
            /*let nextNum = (parsedBody.length - (Number(currentNum) + 10) < 1 ? 0 : (Number(currentNum) + 10))*/

            let response = JSON.parse(JSON.stringify(parsedBody))
            let amount =req.body.amount
            let reference = response.data.reference
            let messengerId = req.body.messenger_id
            let phone = req.body.user_phone_number
            let cusEmail = req.body.user_email
            let responseData = {
                "set_attributes":
                    {
                        "reference": response.data.reference,
                        "account_balance": currencyformatterJs.format((Number(response.account_balance) + Number(amount)), {currency: 'NGN'}),
                        "formatted_amount": currencyformatterJs.format((Number(amount)), {currency: 'NGN'})
                    },
                "messages": [
                    {
                        "attachment": {
                            "type": "template",
                            "payload": {
                                "template_type": "button",
                                "text": "To be sure I got your request. You want to deposit a sum of "+currencyformatterJs.format((Number(amount)), {currency: 'NGN'})+ " into your account?",
                                "buttons": [
                                    {
                                        "type": "web_url",
                                        "url": "https://backupcash-bot.herokuapp.com/deposit?amount="+amount+
                                            "&reference="+reference+"&email="+cusEmail+"&messengerId="+messengerId+"&phone="+phone,
                                        "title": "Make Deposit"
                                    },
                                    {
                                        "type": "show_block",
                                        "block_names": ["Cancel Operation"],
                                        "title": "Cancel"
                                    }
                                ]
                            }
                        }
                    }
                ]
            }
            res.status(201).send(responseData)
        }).catch(function (err) {
        console.log("error",err)
        let responseData;
        if(JSON.parse(JSON.stringify(err.error.message))){
            responseData = {
                "messages": [
                    {
                        "text":  "Sorry, "+name+ ". It seems you don't have an account yet. But I can help you set it up.",
                        "quick_replies": [
                            {
                                "title":"Set up account",
                                "block_names": ["Get user details"]
                            }
                        ]
                    }
                ]
            }
            return  res.status(200).send(responseData)
        }
        else {
            responseData = {
                "messages": [
                    {
                        "text":  "I'm currently unable to complete the transaction. Please, try again later.",
                    }
                ]
            }
            return  res.status(200).send(responseData)
        }
    });
}

const INSTANTSAVE = (req, res) => {
    const options = {
        method: 'POST',
        uri: BASE_URL+'api/v1/bot/user/transaction/instant',
        body: {
            payment_auth: req.body.card_id,
            amount: req.body.amount,
            source: "quick",
            phone: req.body.user_phone_number,
        },
        json: true
    };
    let name = req.body.first_name

    rp(options)
        .then(function (parsedBody) {
            /*let nextNum = (parsedBody.length - (Number(currentNum) + 10) < 1 ? 0 : (Number(currentNum) + 10))*/

            let responseData = {
                "redirect_to_blocks": ["Deposit Success 2"]
            }
            res.status(201).send(responseData)
        }).catch(function (err) {
        console.log("error",JSON.stringify(err))
        let responseData;
        if(JSON.stringify(err.error.message)){
            responseData = {
                "messages": [
                    {
                        "text":  "Sorry, "+name+ ". It seems you don't have an account yet. But I can help you set it up.",
                        "quick_replies": [
                            {
                                "title":"Set up account",
                                "block_names": ["Get user details"]
                            }
                        ]
                    }
                ]
            }
            return  res.status(200).send(responseData)
        }
        else {
            responseData = {
                "messages": [
                    {
                        "text":  "I'm currently unable to complete the transaction. Please, try again later.",
                    }
                ]
            }
            return  res.status(200).send(responseData)
        }
    });
}

const ADDACCOUNT = (req, res) => {
    const options = {
        method: 'POST',
        uri: BASE_URL+'api/v1/bot/user/bank',
        body: {
            phone: req.body.user_phone_number,
            account_number:req.body.account_number,
            bank_code:req.body.bank_code,
        },
        json: true
    };
    let name = req.body.first_name

    rp(options)
        .then(function (parsedBody) {
            /*let nextNum = (parsedBody.length - (Number(currentNum) + 10) < 1 ? 0 : (Number(currentNum) + 10))*/

            let response = JSON.parse(JSON.stringify(parsedBody))
            let amount = 0

            let responseData;
            if(req.body.amount){
                amount = req.body.amount
            }
            // if(amount === 0){
            //     responseData = {
            //         "set_attributes":
            //             {
            //                 "gw_customer_code": parsedBody.data.data.recipient_code,
            //                 "account_number": parsedBody.data.data.details.account_number
            //             },
            //             "redirect_to_blocks": [req.body]
            //     }
            // }else{
            responseData = {
                "set_attributes":
                    {
                        "card_id": parsedBody.data.data.id,
                        "gw_customer_code": parsedBody.data.data.recipient_code,
                        "account_number": parsedBody.data.data.details.account_number,
                        "formatted_withdrawal_amount": currencyformatterJs.format((Number(amount)), {currency: 'NGN'})
                    },
                    "redirect_to_blocks": [req.body.redirection]
                // "messages": [
                //     {
                //         "text":  "Alright "+name+ ", your bank account has been added. Would you like to continue with the withdrawal of "+currencyformatterJs.format((Number(amount)), {currency: 'NGN'}) + "?",
                //         "quick_replies": [
                //             {
                //                 "title":"Proceed",
                //                 "block_names": ["Initiate withdrawal"]
                //             },
                //             {
                //                 "title":"Change amount",
                //                 "block_names": ["Change withdrawal amount"]
                //             },
                //             {
                //                 "title":"Cancel",
                //                 "block_names": ["Cancel transaction"]
                //             }
                //         ]
                //     }
                // ]
            }
        // }
            res.status(201).send(responseData)
        }).catch(function (err) {
        console.log("error",err)
        let responseData;

        if(err.error && err.error.hasOwnProperty("data")){
            if(err.error.data.includes("Account number is invalid")){
                responseData = {
                    "messages": [
                        {
                            "text":  "The account number is incorrect.",
                            "quick_replies": [
                                {
                                    "title":"Add a valid account",
                                    "block_names": ["Get account number"]
                                }
                            ]
                        }
                    ]
                }
            }else  if(err.error.data.includes("Bank is invalid")){
                responseData = {
                    "set_attributes":
                        {
                            "bank_number": 1
                        },
                    "messages": [
                        {
                            "text":  "Bank is invalid",
                            "quick_replies": [
                                {
                                    "title":"Add a valid bank",
                                    "block_names": ["Add bank account"]
                                }
                            ]
                        }
                    ]
                }
            }
        }else {

            responseData = {
                "messages": [
                    {
                        "text": "I'm currently unable to add your account. Please, try again later.",
                    }
                ]
            }
        }
            return  res.status(200).send(responseData)
        /*}*/
    });
}

const convertDate = (withdrawal_date) =>{
    const month = withdrawal_date.split("/")
    let withdrawal;
    let newmonth = 1
    if(month[0] === "January" || month[0] === "01" || month[0] === "1" || month[0] === 1){
        newmonth = '1'
    }else if(month[0] === "February" || month[0] === "02" || month[0] === "2" || month[0] === 2){
        newmonth = "2"
    }else if(month[0] === "March" || month[0] === "03" || month[0] === "3" || month[0] === 3){
        newmonth = "3"
    }else if(month[0] === "April" || month[0] === "04" || month[0] === "4" || month[0] === 4){
        newmonth = "4"
    }else if(month[0] === "May" || month[0] === "05" || month[0] === "5" || month[0] === 5){
        newmonth = "5"
    }else if(month[0] === "June" || month[0] === "06" || month[0] === "6" || month[0] === 6){
        newmonth = "6"
    }else if(month[0] === "July" || month[0] === "07" || month[0] === "7" || month[0] === 7){
        newmonth = "7"
    }else if(month[0] === "August" || month[0] === "08" || month[0] === "8" || month[0] === 8){
        newmonth = "8"
    }else if(month[0] === "September" || month[0] === "09" || month[0] === "9" || month[0] === 9){
        newmonth = "9"
    }else if(month[0] === "October" || month[0] === "10" || month[0] === 10){
        newmonth = "10"
    }else if(month[0] === "November" || month[0] === "11" || month[0] === 11){
        newmonth = "11"
    }else if(month[0] === "December" || month[0] === "12" || month[0] === 12){
        newmonth = "12"
    }
    withdrawal = newmonth.toString() + "/" +month[1].toString()
    return withdrawal

}

const ValidateDate = (arr) =>{


}

const ADDUSERWITHDRAWALDATE = (req, res) => {

    let first_withdrawal = convertDate(req.body.first_withdrawal)
    let second_withdrawal = convertDate(req.body.second_withdrawal)
    let third_withdrawal = convertDate(req.body.third_withdrawal)
    let fourth_withdrawal = convertDate(req.body.fourth_withdrawal)
    let arr = [first_withdrawal, second_withdrawal, third_withdrawal, fourth_withdrawal]
    let quarter = "1st"
    let withdrawal_label = "First Quarter"
    let withdrawalDate = first_withdrawal

    const months = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"]
    const days = ["1", "2", "3", "4", "5", "6", "7", "8", "9","01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12","13", "14", "15", "16", "17",
        "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28","29","30","31"]
    const f1 = months.slice(0, 3)
    const f2 = months.slice(3, 6)
    const f3 = months.slice(6, 9)
    const f4 = months.slice(9, 12)
    let withdrawalObj
    let errMsg = ""
    let responseData
    if(!f1.includes(first_withdrawal.split("/")[0]) || !days.includes(first_withdrawal.split("/")[1])){

        responseData = {
            "redirect_to_blocks": ["wrong f1"]
        }
        return  res.status(201).send(responseData)

    }else if(!f2.includes(second_withdrawal.split("/")[0]) || !days.includes(second_withdrawal.split("/")[1])){

        responseData = {
            "redirect_to_blocks": ["wrong f2"]
        }
        return  res.status(201).send(responseData)

    }else if(!f3.includes(third_withdrawal.split("/")[0]) || !days.includes(third_withdrawal.split("/")[1])){

        responseData = {
            "redirect_to_blocks": ["wrong f3"]
        }
        return  res.status(201).send(responseData)

    }else if(!f4.includes(fourth_withdrawal.split("/")[0]) || !days.includes(fourth_withdrawal.split("/")[1])){

        responseData = {
            "redirect_to_blocks": ["wrong f4"]
        }
        return  res.status(201).send(responseData)

    }else{
        const currentMonth = JSON.stringify(new Date().getMonth() + 1)

        if(f2.includes(currentMonth)){
            quarter = "2nd"
            withdrawal_label = "Second Quarter"
            withdrawalDate = second_withdrawal

        }else if(f3.includes(currentMonth)){
            quarter = "3rd"
            withdrawal_label = "Third Quarter"
            withdrawalDate = third_withdrawal

        }else if(f4.includes(currentMonth)){
            quarter = "4th"
            withdrawal_label = "Fourth Quarter"
            withdrawalDate = fourth_withdrawal
        }

    }


    const options = {
        method: 'POST',
        uri: BASE_URL+'api/v1/bot/user/withdrawalsettings/store',
        body: {
            phone: req.body.user_phone_number,
            label: [req.body.label],
            withdrawal_date: arr
        },
        json: true
    };

    rp(options)
        .then(function (parsedBody) {
            let responseData = {
                "set_attributes":
                    {
                        withdrawal_date: withdrawalDate,
                        quarter: withdrawal_label
                    },
                "redirect_to_blocks": ["Get withdrawal charge"]
            }
            res.status(201).send(responseData)
        }).catch(function (err) {
        let responseData;

            responseData = {
                "messages": [
                    {
                        "text":  "I'm currently unable to add withdrawal dates. Please, try again later.",
                    }
                ]
            }
            return  res.status(200).send(responseData)
    });
}

const USERBANK = (req, res) => {
    const options = {
        method: 'GET',
        uri: BASE_URL+'api/v1/bot/user/bank',
        qs: {
            phone: req.query.user_phone_number,

        },
        json: true
    };
    let name = req.query.first_name
    let amount = req.query.amount

    rp(options)
        .then(function (parsedBody) {


           let responseData;

           if(parsedBody.data.length < 1){
               responseData = {
                   "messages": [
                       {
                           "text":  "Sorry, "+name+ ". I noticed you do not have a bank on this platform yet. Let me help you set it up.",
                           "quick_replies": [
                               {
                                   "title":"Add bank account",
                                   "block_names": ["Select bank"]
                               }
                           ]
                       }
                   ]
               }
           }else{
               let quick = parsedBody.data.map((item, ind) => {
                   return (
                       {
                           "title": item.bank.substring(0, 20),
                           "set_attributes": {
                               "card_id": item.id,
                               "gw_authorization_code": item.gw_authorization_code,
                               "gw_customer_code": item.gw_customer_code,
                               "gw_customer_id": item.gw_customer_id,
                               "account_number": item.bank_number,
                               "channel": item.channel,
                               "formatted_withdrawal_amount": currencyformatterJs.format((Number(amount)), {currency: 'NGN'})
                           },
                           "block_names": [req.query.redirection]
                       }
                   )
               })

               quick.push({
                "title": "Add new bank",
                "set_attributes": {
                    "formatted_withdrawal_amount": currencyformatterJs.format((Number(amount)), {currency: 'NGN'})
                },
                "block_names": ["Add new bank"]
               })

               responseData = {
                   "messages": [
                       {
                           "text":  "Which account will you like to use.",
                           "quick_replies": quick
                       }
                   ]
               }
           }
            res.status(201).send(responseData)
        }).catch(function (err) {
        console.log("error",err)
        let responseData;
        if(JSON.parse(JSON.stringify(err.error.message))){
            responseData = {
                "messages": [
                    {
                        "text":  "Sorry, "+name+ ". It seems you are not a registered user. No worries, I can help you set up your account.",
                        "quick_replies": [
                            {
                                "title":"Set up account",
                                "block_names": ["Get user details"]
                            }
                        ]
                    }
                ]
            }
            return  res.status(200).send(responseData)
        }
        else {
            responseData = {
                "messages": [
                    {
                        "text":  "I'm currently unable to complete the transaction. Please, try again later.",
                    }
                ]
            }
            return  res.status(200).send(responseData)
        }
    });
}

const USERCARD = (req, res) => {
    const options = {
        method: 'GET',
        uri: BASE_URL+'api/v1/bot/user/card',
        qs: {
            phone: req.query.user_phone_number,

        },
        json: true
    };
    let name = req.query.first_name
    let amount = req.query.amount

    rp(options)
        .then(function (parsedBody) {


           let responseData;

           if(parsedBody.data.length < 1){
               let text = { 
                   "text":  "Sorry, "+name+ ". I noticed you do not have a card on this platform yet. Let me help you set it up.",
                    "quick_replies": [
                        {
                            "title":"Add a card",
                            "set_attributes": {
                                "formatted_amount": currencyformatterJs.format((Number(amount)), {currency: 'NGN'})
                            },
                            "block_names": ["Set up card"]
                        }
                    ]}
               if(req.query.redirection === "Instant Save"){
                   text = {
                    "set_attributes": {
                        "formatted_amount": currencyformatterJs.format((Number(amount)), {currency: 'NGN'})
                    },
                    "redirect_to_blocks": ["Authorize card"]
                   }
                   responseData = text

               }else{
                responseData = {
                    "messages": [
                        text
                    ]
                }
                   
               }
               
           }else{
               let quick = parsedBody.data.map((item, ind) => {
                   return (
                       {
                           "title": item.brand + " (*** " + item.last4+ ")",
                           "set_attributes": {
                               "card_id": item.id,
                               "gw_authorization_code": item.gw_authorization_code,
                               "gw_customer_code": item.gw_customer_code,
                               "gw_customer_id": item.gw_customer_id,
                               "channel": item.channel,
                               "formatted_amount": currencyformatterJs.format((Number(amount)), {currency: 'NGN'})
                           },
                           "block_names": [req.query.redirection]
                       }
                   )
               })
               if(req.query.redirection === "Instant Save"){
                quick.push({
                    "title": "Use a new card",
                    "set_attributes": {
                       "formatted_amount": currencyformatterJs.format((Number(amount)), {currency: 'NGN'})
                    },
                    "block_names": ["Authorize card"]
                   })
                
                }else{
                    quick.push({
                        "title": "Add a new card",
                        "set_attributes": {
                           "formatted_amount": currencyformatterJs.format((Number(amount)), {currency: 'NGN'})
                        },
                        "block_names": ["Set up card"]
                       })
                    
                }
              

               responseData = {
                   "messages": [
                       {
                           "text":  "Which card should the transaction amount be deducted from?",
                           "quick_replies": quick
                       }
                   ]
               }
           }
            res.status(201).send(responseData)
        }).catch(function (err) {
        console.log("error",err)
        let responseData;
        if(JSON.parse(JSON.stringify(err.error.message))){
            responseData = {
                "messages": [
                    {
                        "text":  "Sorry, "+name+ ". It seems you are not a registered user. No worries, I can help you set up your account.",
                        "quick_replies": [
                            {
                                "title":"Set up account",
                                "block_names": ["Get user details"]
                            }
                        ]
                    }
                ]
            }
            return  res.status(200).send(responseData)
        }
        else {
            responseData = {
                "messages": [
                    {
                        "text":  "I'm currently unable to complete the transaction. Please, try again later.",
                    }
                ]
            }
            return  res.status(200).send(responseData)
        }
    });
}

const GETWITHDRAWALDATE = (req, res) => {

     console.log(req.body)
    const options = {
        method: 'POST',
        uri: BASE_URL+'api/v1/bot/user/withdrawalsettings',
        body: {
            phone: req.body.user_phone_number,
        },
        json: true
    };


    rp(options)
        .then(function (parsedBody) {
            const currentMonth = JSON.stringify(new Date().getMonth() + 1)
            let quarter = "1st"
            let withdrawal_label = "First Quarter"
            let button_label = "F1"
            const months = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"]
            const f1 = months.slice(0, 3)
            const f2 = months.slice(3, 6)
            const f3 = months.slice(6, 9)
            const f4 = months.slice(9, 12)
            let withdrawalObj

            if(f2.includes(currentMonth)){
                quarter = "2nd"
                withdrawal_label = "Second Quarter"
                button_label = "F2"
                withdrawalObj = parsedBody.data.filter((item, i) =>{
                    return f2.includes(item.withdrawal_date.split("/")[0])
                })
            }else if(f3.includes(currentMonth)){
                quarter = "3rd"
                withdrawal_label = "Third Quarter"
                button_label = "F3"
                withdrawalObj = parsedBody.data.filter((item, i) =>{
                    return f3.includes(item.withdrawal_date.split("/")[0])
                })
            }else if(f4.includes(currentMonth)){
                quarter = "4th"
                withdrawal_label = "Fourth Quarter"
                button_label = "F4"
                withdrawalObj = parsedBody.data.filter((item, i) =>{
                    return f4.includes(item.withdrawal_date.split("/")[0])
                })
            }else{
                withdrawalObj = parsedBody.data.filter((item, i) =>{
                    return f1.includes(item.withdrawal_date.split("/")[0])
                })
            }

            let responseData;

            if(parsedBody.owner === "admin"){
                let quick = []
                    /*parsedBody.data.map((item, ind) => {
                    return (
                        {
                            "title": item.label.substring(0, 20),
                            "set_attributes": {
                                "withdrawal_label": item.label,
                                "withdrawal_date": item.withdrawal_date,
                                "withdrawal_id": item.id
                            },
                            "block_names": [item.label]
                        }
                    )
                })*/

                quick.push({
                    "title": "Set custom date",
                    "block_names": ["Set custom withdrawal date"]
                }, {
                    "title": "Use default dates",
                    "set_attributes": {
                        "first_withdrawal": parsedBody.data[0].withdrawal_date,
                        "second_withdrawal": parsedBody.data[1].withdrawal_date,
                        "third_withdrawal": parsedBody.data[2].withdrawal_date,
                        "fourth_withdrawal": parsedBody.data[3].withdrawal_date,
                        "withdrawal_label": withdrawalObj[0].label,
                        "withdrawal_date": withdrawalObj[0].withdrawal_date,
                        "withdrawal_id": withdrawalObj[0].id,
                        "quarter": quarter
                    },
                    "block_names": ["Set admin dates"]
                })

                responseData = {
                    "messages": [
                        {
                            "text":  "You do not have a set withdrawal date yet. Your default free withdrawal dates for each quarter are "+
                                parsedBody.data[0].withdrawal_date+", "+ parsedBody.data[1].withdrawal_date+", "+ parsedBody.data[2].withdrawal_date+
                                ", "+ parsedBody.data[3].withdrawal_date+". You can still set custom dates. Please note that custom dates can be set once and ant transaction done outside these dates will attract some charges..",
                            "quick_replies": quick
                        }
                    ]
                }
            }
            else{

                if(withdrawalObj.length < 1){
                    responseData = {
                        "messages": [
                            {
                                "text":  "You do not have a withdrawal date for the "+ quarter +" quarter yet. Please set your "+ quarter +" quarter withdrawal date. Note that any transaction done outside this date will attract some charges.",
                                "quick_replies": [{
                                    "title": "Set withdrawal date",
                                    "set_attributes": {
                                        "withdrawal_label": withdrawal_label
                                    },
                                    "block_names": [button_label]
                                }]
                            }
                        ]
                    }
                }else {
                    responseData = {
                        "set_attributes":
                            {
                                "withdrawal_date": withdrawalObj[0].withdrawal_date
                            },
                        "messages": [
                            {
                                "text": "Your withdrawal date is " + withdrawalObj[0].withdrawal_date + ". Please note that withdrawal on any other day attracts a charge. Do you wish to proceed?",
                                "quick_replies": [
                                    {
                                        "title": "Proceed",
                                        "set_attributes": {
                                            "withdrawal_label": withdrawalObj[0].label,
                                            "withdrawal_date": withdrawalObj[0].withdrawal_date,
                                            "withdrawal_id": withdrawalObj[0].id
                                        },
                                        "block_names": ["Get charge"]
                                    }
                                ]
                            }
                        ]
                    }
                }
            }


            
            res.status(201).send(responseData)
        }).catch(function (err) {
        let responseData;

        responseData = {
            "messages": [
                {
                    "text":  "I'm currently unable to fetch withdrawal date. Please, try again later.",
                }
            ]
        }
        return  res.status(200).send(responseData)

    });
}

const GETBANKS = (req, res) => {
    const options = {
        method: 'GET',
        uri: 'https://api.paystack.co/bank',
        qs: {

        },
        headers: {Authorization: ""},
        json: true
    };
    let currentNum = Number(req.query.bank_number) === 1 ? Number(req.query.bank_number) - 1 : Number(req.query.bank_number)


    rp(options)
        .then(function (parsedBody) {
            let nextNum = (parsedBody.data.length - (Number(currentNum)+10) < 1 ? 0 : (Number(currentNum)+10))


            let responseData;

            if(parsedBody.data.length > 0) {

                let quick = parsedBody.data.slice(Number(currentNum), Number(currentNum) + 10).map((item, ind) => {
                    return (
                        {
                            "title": item.name.substring(0, 20),
                            "set_attributes": {
                                "bank_name": item.name,
                                "bank_code": item.code,
                            },
                            "block_names": ["Get account number"]
                        }
                    )
                })
                if(nextNum > 0){
                    quick.push({
                        "title": "Get more banks",
                        "block_names": ["Add bank account"]
                    })
                }

                responseData = {
                    "set_attributes":
                        {
                            "bank_number": nextNum
                        },
                    "messages": [
                        {
                            "text": "...",
                            "quick_replies": quick


                        }
                    ]
                }
            }else{
                responseData = {
                    "messages": [
                        {
                            "text":  "I'm currently unable to retrieve banks. Please, try again later.",
                        }
                    ]
                }
            }

            res.status(201).send(responseData)
        }).catch(function (err) {
        let responseData;
        responseData = {
                "messages": [
                    {
                        "text":  "I'm currently unable to complete the transaction. Please, try again later.",
                    }
                ]
            }
            return  res.status(200).send(responseData)
    });
}

const GETPENALTY = (req, res) => {
    let withdrawal_date = req.query.withdrawal_date
    const month = withdrawal_date.split("/")
    let withdrawal;
    let newmonth = 1
    console.log(month[0], typeof month[0])
    if(month[0] === "January" || month[0] === "1"){
        newmonth = '1'
    }else if(month[0] === "February" || month[0] === "2"){
        newmonth = "2"
    }else if(month[0] === "March" || month[0] === "3"){
        newmonth = "3"
    }else if(month[0] === "April" || month[0] === "4"){
        newmonth = "4"
    }else if(month[0] === "May" || month[0] === "5"){
        newmonth = "5"
    }else if(month[0] === "June" || month[0] === "6"){
        newmonth = "6"
    }else if(month[0] === "July" || month[0] === "7"){
        newmonth = "7"
    }else if(month[0] === "August" || month[0] === "8"){
        newmonth = "8"
    }else if(month[0] === "September" || month[0] === "9"){
        newmonth = "9"
    }else if(month[0] === "October" || month[0] === "10"){
        newmonth = "10"
    }else if(month[0] === "November" || month[0] === "11"){
        newmonth = "11"
    }else if(month[0] === "December" || month[0] === "12"){
        newmonth = "12"
    }
    withdrawal = newmonth.toString() + "/" +month[1].toString()

    let day = moment(new Date()).format("M/DD")
   /* console.log(day, withdrawal)
    console.log(day === withdrawal)*/

    const options = {
        method: 'GET',
        uri: BASE_URL+'api/v1/bot/user/withdrawalsettings/penalty',
        qs: {

        },
        json: true
    };


    rp(options)
        .then(function (parsedBody) {



            let responseData;

            if(day === withdrawal){
                responseData = {
                    "redirect_to_blocks": ["Set charge"]
                }
            }else {

                responseData = {
                    "set_attributes":
                        {
                            "withdraw_penalty": parsedBody.data.withdraw_penalty
                        },
                    "messages": [
                        {
                            "text": "Your withdrawal penalty is "+ parsedBody.data.withdraw_penalty + "%. Will you like to continue?",
                            "quick_replies": [
                                {
                                    "title": "Proceed",
                                    "block_names": ["Get penalty point"]
                                },
                                {
                                    "title": "Cancel withdrawal",
                                    "block_names": ["Cancel transaction"]
                                }
                            ]


                        }
                    ]
                }
            }
            res.status(201).send(responseData)
        }).catch(function (err) {
        let responseData;
        responseData = {
                "messages": [
                    {
                        "text":  "I'm currently unable to complete the transaction. Please, try again later.",
                    }
                ]
            }
            return  res.status(200).send(responseData)
    });
}

const SETWITHDRAWALPIN = (req, res) => {
   
    const options = {
        method: 'POST',
        uri: BASE_URL+'api/v1/bot/user/withdrawalpin',
        body: {phone: req.body.phone,
                withdrawal_pin : req.body.withdrawal_pin},
        json: true
    };

    rp(options)
        .then(function (parsedBody) {

            let responseData;

            responseData = {
                "messages": [
                    {
                        "text":  "Your pin has been set to "+parsedBody.data.pin,
                    }
                ],
                "redirect_to_blocks": ["Withdraw"]
            }
            res.status(201).send(responseData)

        }).catch(function (err) {
        let responseData;
        
            responseData = {
                "redirect_to_blocks": ["Failed pin set"]
            }
            return res.status(200).send(responseData)
        
    });
}

const MAKEWITHDRAWAL = (req, res) => {
    /*"facebook_messenger_id":"23467876788889",
    "source":"central_vault",
    "withdrawal_pin":"1778",
    "bank_account":"RCP_qae0t4nu3qg4w7e",
    "withdraw_amount":2000,
    "penalty_from":"amount_to_withdraw"*/

    let withdrawal_date = req.body.withdrawal_date
    const month = withdrawal_date.split("/")
    let withdrawal;
    let newmonth = 1
    console.log(month[0], typeof month[0])
    if(month[0] === "January" || month[0] === "1"){
        newmonth = '1'
    }else if(month[0] === "February" || month[0] === "2"){
        newmonth = "2"
    }else if(month[0] === "March" || month[0] === "3"){
        newmonth = "3"
    }else if(month[0] === "April" || month[0] === "4"){
        newmonth = "4"
    }else if(month[0] === "May" || month[0] === "5"){
        newmonth = "5"
    }else if(month[0] === "June" || month[0] === "6"){
        newmonth = "6"
    }else if(month[0] === "July" || month[0] === "7"){
        newmonth = "7"
    }else if(month[0] === "August" || month[0] === "8"){
        newmonth = "8"
    }else if(month[0] === "September" || month[0] === "9"){
        newmonth = "9"
    }else if(month[0] === "October" || month[0] === "10"){
        newmonth = "10"
    }else if(month[0] === "November" || month[0] === "11"){
        newmonth = "11"
    }else if(month[0] === "December" || month[0] === "12"){
        newmonth = "12"
    }
    withdrawal = newmonth.toString() + "/" +month[1].toString()

    let day = moment(new Date()).format("M/DD")
    let penalty

    if(req.body.penalty_from === "Amount to withdraw"){
        penalty = "amount_to_withdraw"
    }else{
        penalty = "central_vault"
    }

    let bodyData;

    if(day === withdrawal){
        bodyData ={
            source:req.body.withdrawal_point === "Backup stash"? "backup_stash":"central_vault",
            withdrawal_pin: req.body.withdrawal_pin,
            phone: req.body.user_phone_number,
            bank_account: req.body.bank_account,
            withdraw_amount: req.body.withdraw_amount
        }
    }else{
        bodyData ={
            source: req.body.withdrawal_point === "Backup stash"? "backup_stash":"central_vault",
            withdrawal_pin: req.body.withdrawal_pin,
            phone: req.body.user_phone_number,
            bank_account: req.body.bank_account,
            withdraw_amount: req.body.withdraw_amount,
            penalty_from: penalty,
        }
    }


    const options = {
        method: 'POST',
        uri: BASE_URL+'api/v1/bot/user/withdrawal',
        body: bodyData,
        json: true
    };

    rp(options)
        .then(function (parsedBody) {
            /*let nextNum = (parsedBody.length - (Number(currentNum) + 10) < 1 ? 0 : (Number(currentNum) + 10))*/

            let responseData;

            responseData = "Withdrawal Success"
            res.status(200).send(responseData)

        }).catch(function (err) {
        let responseData;
        if(JSON.parse(JSON.stringify(err.error.message)).includes("withdrawal pin mismatch")){
            responseData = "Withdrawal pin mismatch"
            return  res.status(200).send(responseData)
        }else if(JSON.parse(JSON.stringify(err.error.message)).includes("Insufficient amount")){
            responseData = "Insufficient amount"
            return  res.status(200).send(responseData)
        }else if(JSON.parse(JSON.stringify(err.error.message)).includes("withdrawal pin not set")){
            responseData = "Set withdrawal pin"
            return  res.status(200).send(responseData)
        }else if(JSON.parse(JSON.stringify(err.error.message)).includes("withdrawal amount must be greater than")){
            responseData = "Withdrawal amount small"
            return  res.status(200).send(responseData)
        }else {
            responseData = "Failed withdrawal"
            return res.status(200).send(responseData)
        }
    });
}

const GETLOCKEDINTEREST = (req, res) => {
    const options = {
        method: 'GET',
        uri: BASE_URL+'api/v1/bot/user/locked/settings',
        qs: {
            phone: req.query.phone,
            amount : req.query.amount,
            tenure: req.query.tenure
        },
        json: true
    };

    rp(options)
        .then(function (parsedBody) {


           let responseData;
           let interest_amount = (Number(req.query.amount)*(Number(parsedBody.data.interest)/100))

            responseData = {
                "set_attributes":
                    {
                        "interest_per":parsedBody.data.interest,
                        "interest_amount": interest_amount,
                        "formatted_interest_amount": currencyformatterJs.format((Number(interest_amount)), {currency: 'NGN'})
                        
                    },
                    "redirect_to_blocks": ["Confirm locked saving"]
               }
            res.status(201).send(responseData)
        }).catch(function (err) {
        console.log("error",err)
        let responseData;
         responseData = {
                "messages": [
                    {
                        "text":  "I'm currently unable to complete the transaction. Please, try again later.",
                    }
                ]
            }
            return  res.status(200).send(responseData)
        
    });
}

const LOG = (req, res, next) => {
    const options = {
        method: 'post',
        uri: BASE_URL+'api/v1/bot/user/transaction/verify',
        body: {
            ref: req.body.ref, type: req.body.type,
            phone: req.body.user_phone_number,
        },
        json: true
    };

    rp(options)
        .then(function (parsedBody) {
            res.status(200).send(parsedBody)
        })
        .catch(error => res.status(400).send(error));
}

const GETUSERDETAILS = (req, res) => {
    const options = {
        method: 'POST',
        uri: BASE_URL+'api/v1/bot/user/info',
        body: {
            phone: req.query.user_phone_number,
        },
        json: true
    };

    // "central_vault": 0,
    // "backup_stash": 0,
    // "interest": 0,
    // "locked": 0,
    // "backup_goal": 0,
   

    rp(options)
        .then(function (parsedBody) {


           let responseData;

            responseData = {
                "set_attributes":
                    {
                        "central_vault": currencyformatterJs.format((Number(parsedBody.central_vault)), {currency: 'NGN'}),
                        "backup_stash": currencyformatterJs.format((Number(parsedBody.backup_stash)), {currency: 'NGN'}),
                        "locked_savings": currencyformatterJs.format((Number(parsedBody.locked)), {currency: 'NGN'}),
                        "interest": currencyformatterJs.format((Number(parsedBody.interest)), {currency: 'NGN'}),
                        "backup_savings": currencyformatterJs.format((Number(parsedBody.backup_goal)), {currency: 'NGN'}),
                        "user_phone_number": parsedBody.user.phone,
                        "user_email": parsedBody.user.email,
                        "referral_code": parsedBody.user.referral_code,
                        "referral_link": parsedBody.user.referral_link,
                        "user_first_name": parsedBody.user.name,
                        "user_last_name": parsedBody.user.last_name
                    },
                    "redirect_to_blocks": ["User dashboard"]
               }
            res.status(201).send(responseData)
        }).catch(function (err) {
        console.log("error",err)
        let responseData;
        if(JSON.parse(JSON.stringify(err.error.message))){
            responseData = {
                "messages": [
                    {
                        "text":  "Sorry, "+req.query.name+ ". It seems you are not a registered user. No worries, I can help you set up your account.",
                        "quick_replies": [
                            {
                                "title":"Set up account",
                                "block_names": ["Get user details"]
                            }
                        ]
                    }
                ]
            }
            return  res.status(200).send(responseData)
        }
        else {
            responseData = {
                "messages": [
                    {
                        "text":  "I'm currently unable to complete the transaction. Please, try again later.",
                    }
                ]
            }
            return  res.status(200).send(responseData)
        }
    });
}

const USERBANKSTEADY = (req, res) => {
    const options = {
        method: 'GET',
        uri: BASE_URL+'api/v1/bot/user/bank',
        qs: {
            phone: req.query.user_phone_number,

        },
        json: true
    };
    let name = req.query.first_name

    rp(options)
        .then(function (parsedBody) {


           let responseData;

           if(parsedBody.data.length < 1){
               responseData = {
                   "messages": [
                       {
                           "text":  "Sorry, "+name+ ". I noticed you do not have a bank on this platform yet. Let me help you set it up.",
                           "quick_replies": [
                               {
                                   "title":"Add bank account",
                                   "block_names": ["Select bank"]
                               }
                           ]
                       }
                   ]
               }
           }else{
               let quick = parsedBody.data.map((item, ind) => {
                   return (
                       {
                           "title": item.bank.substring(0, 20),
                           "set_attributes": {
                               "gw_authorization_code": item.gw_authorization_code,
                               "gw_customer_code": item.gw_customer_code,
                               "gw_customer_id": item.gw_customer_id,
                               "account_number": item.bank_number,
                               "channel": item.channel
                           },
                           "block_names": ["Confirm Save"]
                       }
                   )
               })

               responseData = {
                   "messages": [
                       {
                           "text":  "Which account will you like to use.",
                           "quick_replies": quick
                       }
                   ]
               }
           }
            res.status(201).send(responseData)
        }).catch(function (err) {
        console.log("error",err)
        let responseData;
        if(JSON.parse(JSON.stringify(err.error.message))){
            responseData = {
                "messages": [
                    {
                        "text":  "Sorry, "+name+ ". It seems you are not a registered user. No worries, I can help you set up your account.",
                        "quick_replies": [
                            {
                                "title":"Set up account",
                                "block_names": ["Get user details"]
                            }
                        ]
                    }
                ]
            }
            return  res.status(200).send(responseData)
        }
        else {
            responseData = {
                "messages": [
                    {
                        "text":  "I'm currently unable to complete the transaction. Please, try again later.",
                    }
                ]
            }
            return  res.status(200).send(responseData)
        }
    });
}

const CREATESTEADYSAVE = (req, res) => {
    try{
    let start_date = req.body.steady_start_date
    console.log(new Date(start_date),  new Date())
    const month = start_date.split("-")
    const dMon = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
    const days = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12,13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28,29,30,31]
    if(month.length !== 3 || month[0].length !== 4 || !dMon.includes(Number(month[1])) || !days.includes(Number(month[2]))|| 
    (new Date(start_date) < new Date())){
        let responseData = {
            "redirect_to_blocks": ["Re-enter start date"]
        }
       return res.status(201).send(responseData)
    }
    let time;
    if(req.body.hour_of_day === "1 a.m"){
        time = "1"
    }else if(req.body.hour_of_day === "2 a.m"){
        time = "2"
    }else if(req.body.hour_of_day === "3 a.m"){
        time = "3"
    }else if(req.body.hour_of_day === "4 a.m"){
        time = "4"
    }else if(req.body.hour_of_day === "5 a.m"){
        time = "5"
    }else if(req.body.hour_of_day === "6 a.m"){
        time = "6"
    }else if(req.body.hour_of_day === "7 a.m"){
        time = "7"
    }else if(req.body.hour_of_day === "8 a.m"){
        time = "8"
    }else if(req.body.hour_of_day === "9 a.m"){
        time = "9"
    }else if(req.body.hour_of_day === "10 a.m"){
        time = "10"
    }else if(req.body.hour_of_day === "11 a.m"){
        time = "11"
    }else if(req.body.hour_of_day === "12 p.m"){
        time = "12"
    }else if(req.body.hour_of_day === "1 p.m"){
        time = "13"
    }else if(req.body.hour_of_day === "2 p.m"){
        time = "14"
    }else if(req.body.hour_of_day === "3 p.m"){
        time = "15"
    }else if(req.body.hour_of_day === "4 p.m"){
        time = "16"
    }else if(req.body.hour_of_day === "5 p.m"){
        time = "17"
    }else if(req.body.hour_of_day === "6 p.m"){
        time = "18"
    }else if(req.body.hour_of_day === "7 p.m"){
        time = "19"
    }else if(req.body.hour_of_day === "8 p.m"){
        time = "20"
    }else if(req.body.hour_of_day === "9 p.m"){
        time = "21"
    }else if(req.body.hour_of_day === "10 p.m"){
        time = "22"
    }else if(req.body.hour_of_day === "11 p.m"){
        time = "23"
    }else if(req.body.hour_of_day === "12 a.m"){
        time = "00"
    }
    // "hour_of_day": "10",
    // "payment_auth": "1",
    // "contribution": "1000",
    // "phone":"085763738889",
    // "start_date":"2019-05-19",
    // "title":"Steady Save",
    // "frequency":"daily"
    let data = {};
    if(req.body.payment_frequency === "Daily"){
        data = {
            hour_of_day: time,
            payment_auth: req.body.card_id,
            contribution: req.body.steady_amount,
            phone: req.body.phone,
            start_date: start_date,
            title: req.body.title,
            frequency: req.body.payment_frequency.toLowerCase()
        }
    }else if(req.body.payment_frequency === "Weekly"){
        let weekday;
    if(req.body.day_of_week === "Monday"){
        weekday = "1"
    }else if(req.body.day_of_week === "Tuesday"){
        weekday = "2"
    }else if(req.body.day_of_week === "Wednesday"){
        weekday = "3"
    }else if(req.body.day_of_week === "Thursday"){
        weekday = "4"
    }else if(req.body.day_of_week === "Friday"){
        weekday = "5"
    }else if(req.body.day_of_week === "Saturday"){
        weekday = "6"
    }else {
        weekday = "7"
    }
        data = {
            hour_of_day: time,
            day_of_week: weekday,
            payment_auth: req.body.card_id,
            contribution: req.body.steady_amount,
            phone: req.body.phone,
            start_date: start_date,
            title: req.body.title,
            frequency: req.body.payment_frequency.toLowerCase()
        }
    }else if(req.body.payment_frequency === "Monthly"){
        data = {
            hour_of_day: time,
            day_of_month: req.body.day_of_month,
            payment_auth: req.body.card_id,
            contribution: req.body.steady_amount,
            phone: req.body.phone,
            start_date: start_date,
            title: req.body.title,
            frequency: req.body.payment_frequency.toLowerCase()
        }
    }else{

    }
    console.log(data)
    


    const options = {
        method: 'POST',
        uri: BASE_URL+'api/v1/bot/user/steadysave',
        body: data,
        json: true
    };

    rp(options)
        .then(function (parsedBody) {
            

            let responseData = {
                "redirect_to_blocks": ["Steady save success"]
            }
            res.status(201).send(responseData)
        }).catch(function (err) {
        console.log("error",err)
        let responseData;

            responseData = {
                "messages": [
                    {
                        "text":  "I'm currently unable to add your account. Please, try again later.",
                    }
                ]
            }
            return  res.status(200).send(responseData)
        /*}*/

    });
}catch(err){
    let responseData;

    responseData = {
        "messages": [
            {
                "text":  "I'm currently unable to create your steady save. Please, try again later.",
            }
        ]
    }
    return  res.status(200).send(responseData)
}
}

const CREATEBACKUPGOAL = (req, res) => {
    try{
    let start_date = req.body.goal_start_date
    let end_date = req.body.goal_end_date
    const month = start_date.split("-")
    const end = end_date.split("-")
    const dMon = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
    const days = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12,13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28,29,30,31]
    if(month.length !== 3 || month[0].length !== 4 || !dMon.includes(Number(month[1])) || !days.includes(Number(month[2])) || 
   end.length !== 3 || end[0].length !== 4 || !dMon.includes(Number(end[1])) || !days.includes(Number(end[2])) ||
   (new Date(start_date) > new Date(end_date)) || 
    (new Date(start_date) < new Date())){
        let responseData = {
            "redirect_to_blocks": ["Re-enter start date Goal"]
        }
       return res.status(201).send(responseData)
    }
    let time;
    if(req.body.hour_of_day === "1 a.m"){
        time = "1"
    }else if(req.body.hour_of_day === "2 a.m"){
        time = "2"
    }else if(req.body.hour_of_day === "3 a.m"){
        time = "3"
    }else if(req.body.hour_of_day === "4 a.m"){
        time = "4"
    }else if(req.body.hour_of_day === "5 a.m"){
        time = "5"
    }else if(req.body.hour_of_day === "6 a.m"){
        time = "6"
    }else if(req.body.hour_of_day === "7 a.m"){
        time = "7"
    }else if(req.body.hour_of_day === "8 a.m"){
        time = "8"
    }else if(req.body.hour_of_day === "9 a.m"){
        time = "9"
    }else if(req.body.hour_of_day === "10 a.m"){
        time = "10"
    }else if(req.body.hour_of_day === "11 a.m"){
        time = "11"
    }else if(req.body.hour_of_day === "12 p.m"){
        time = "12"
    }else if(req.body.hour_of_day === "1 p.m"){
        time = "13"
    }else if(req.body.hour_of_day === "2 p.m"){
        time = "14"
    }else if(req.body.hour_of_day === "3 p.m"){
        time = "15"
    }else if(req.body.hour_of_day === "4 p.m"){
        time = "16"
    }else if(req.body.hour_of_day === "5 p.m"){
        time = "17"
    }else if(req.body.hour_of_day === "6 p.m"){
        time = "18"
    }else if(req.body.hour_of_day === "7 p.m"){
        time = "19"
    }else if(req.body.hour_of_day === "8 p.m"){
        time = "20"
    }else if(req.body.hour_of_day === "9 p.m"){
        time = "21"
    }else if(req.body.hour_of_day === "10 p.m"){
        time = "22"
    }else if(req.body.hour_of_day === "11 p.m"){
        time = "23"
    }else if(req.body.hour_of_day === "12 a.m"){
        time = "00"
    }
    // "hour_of_day": "10",
    // "payment_auth": "1",
    // "contribution": "1000",
    // "phone":"085763738889",
    // "start_date":"2019-05-19",
    // "title":"Steady Save",
    // "frequency":"daily"

    // "hour_of_day": "10",
    // "payment_auth": "1",
    // "contribution": "1000",
    // "phone":"085763738889",
    // "start_date":"2019-05-19",
    // "end_date":"2019-05-20",
    // "title":"Summer",
    // "frequency":"daily",
    // "goal_amount":"20000"

    let data = {};
    if(req.body.payment_frequency === "Daily"){
        data = {
            hour_of_day: time,
            payment_auth: req.body.card_id,
            contribution: req.body.goal_amount,
            phone: req.body.phone,
            start_date: start_date,
            end_date: end_date,
            title: req.body.title,
            goal_amount:req.body.backup_goal,
            frequency: req.body.payment_frequency.toLowerCase()
        }
    }else if(req.body.payment_frequency === "Weekly"){
        let weekday;
    if(req.body.day_of_week === "Monday"){
        weekday = "1"
    }else if(req.body.day_of_week === "Tuesday"){
        weekday = "2"
    }else if(req.body.day_of_week === "Wednesday"){
        weekday = "3"
    }else if(req.body.day_of_week === "Thursday"){
        weekday = "4"
    }else if(req.body.day_of_week === "Friday"){
        weekday = "5"
    }else if(req.body.day_of_week === "Saturday"){
        weekday = "6"
    }else {
        weekday = "7"
    }
        data = {
            hour_of_day: time,
            day_of_week: weekday,
            payment_auth: req.body.card_id,
            contribution: req.body.goal_amount,
            phone: req.body.phone,
            start_date: start_date,
            end_date: end_date,
            title: req.body.title,
            goal_amount: req.body.backup_goal,
            frequency: req.body.payment_frequency.toLowerCase()
        }
    }else if(req.body.payment_frequency === "Monthly"){
        data = {
            hour_of_day: time,
            day_of_month: req.body.day_of_month,
            payment_auth: req.body.card_id,
            contribution: req.body.goal_amount,
            phone: req.body.phone,
            start_date: start_date,
            end_date: end_date,
            title: req.body.title,
            goal_amount: req.body.backup_goal,
            frequency: req.body.payment_frequency.toLowerCase()
        }
    }else{

    }
    console.log(data)
    


    const options = {
        method: 'POST',
        uri: BASE_URL+'api/v1/bot/user/backupgoal',
        body: data,
        json: true
    };

    rp(options)
        .then(function (parsedBody) {
            

            let responseData = {
                "redirect_to_blocks": ["Goal save success"]
            }
            res.status(201).send(responseData)
        }).catch(function (err) {
        console.log("error",err)
        let responseData;

            responseData = {
                "messages": [
                    {
                        "text":  "I'm currently unable to create your backup goal. Please, try again later.",
                    }
                ]
            }
            return  res.status(200).send(responseData)
        /*}*/

    });
}catch(err){
    let responseData;

    responseData = {
        "messages": [
            {
                "text":  "I'm currently unable to add your account. Please, try again later.",
            }
        ]
    }
    return  res.status(200).send(responseData)
}
}

const GETALLSTEADYSAVE = (req, res) => {
    const options = {
        method: 'GET',
        uri: BASE_URL+'api/v1/bot/user/steadysave',
        qs: {
            phone: req.query.phone
        },
        headers: {Authorization: ""},
        json: true
    };
    let currentNum = Number(req.query.steady_number) === 1 ? Number(req.query.steady_number) - 1 : Number(req.query.steady_number)


    rp(options)
        .then(function (parsedBody) {
            let nextNum = (parsedBody.data.length - (Number(currentNum)+10) < 1 ? 0 : (Number(currentNum)+10))


            let responseData;

            const img = "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e4/Letter_s.svg/1200px-Letter_s.svg.png"

            if(parsedBody.data.length > 0) {

                if(parsedBody.data.length > 1){

                
                let quick = parsedBody.data.slice(Number(currentNum), Number(currentNum) + 4).map((item, ind) => {
                    let subtitle = item.frequency === "daily" ? ( "Daily at " + item.hour_of_day+ " hrs ") : 
                    item.frequency === "weekly" ? ("Every " + item.day_of_week+ " day of the week at "+ item.hour_of_day+ " hrs ") :
                    item.frequency === "monthly" ? ("Every " + item.day_of_month+ " day of the month at "+ item.hour_of_day+ " hrs ") : ""
                    return (
                        {
                            "title":item.title,
                            "image_url":img,
                            "subtitle":subtitle,
                            "buttons":[
                                {
                                    "type": "show_block",
                                    "set_attributes":
                                        {
                                            "steady_save_id": item.id,
                                            "user_save_title": item.title,
                                            "save_subtitle": subtitle,
                                            "user_save_start": currencyformatterJs.format((Number(item.start_amount)), {currency: 'NGN'}),
                                            "user_save_start_date": item.start_date
                                        },
                                    "block_names": ["Show steady details"],
                                    "title": "Show Details"
                                }
                            ]
                          }
                    )
                })

                responseData = {
                    "set_attributes":
                        {
                            "steady_number": nextNum
                        },
                    "messages": [
                        {
                            "attachment":{
                              "type":"template",
                              "payload":{
                                "template_type":"list",
                                "top_element_style":"compact",
                                "elements": quick
                              }
                            }
                          }
                    ]
                }
            }else{
                let quick = parsedBody.data.slice(Number(currentNum), Number(currentNum) + 4).map((item, ind) => {
                    let subtitle = item.frequency === "daily" ? ( "Daily at " + item.hour_of_day+ " hrs ") : 
                    item.frequency === "weekly" ? ("Every " + item.day_of_week+ " day of the week at "+ item.hour_of_day+ " hrs ") :
                    item.frequency === "monthly" ? ("Every " + item.day_of_month+ " day of the month at "+ item.hour_of_day+ " hrs ") : ""
                    return (
                        {
                            "title":item.title,
                            "image_url":img,
                            "subtitle":subtitle,
                            "buttons":[
                                {
                                    "type": "show_block",
                                    "set_attributes":
                                        {
                                            "steady_save_id": item.id,
                                            "user_save_title": item.title,
                                            "save_subtitle": subtitle,
                                            "user_save_start": currencyformatterJs.format((Number(item.start_amount)), {currency: 'NGN'}),
                                            "user_save_start_date": item.start_date
                                        },
                                    "block_names": ["Show steady details"],
                                    "title": "Show Details"
                                }
                            ]
                          }
                    )
                })

                responseData = {
                    "set_attributes":
                        {
                            "steady_number": nextNum
                        },
                        "messages": [
                            {
                              "attachment":{
                                "type":"template",
                                "payload":{
                                  "template_type":"generic",
                                  "image_aspect_ratio": "square",
                                  "elements":quick
                                }
                              }
                            }
                          ]
                }
            
            }
            }else{
                responseData = {
                    "messages": [
                        {
                            "text":  "You do not have any steady save yet.",
                        }
                    ]
                }
            }

            res.status(201).send(responseData)
        }).catch(function (err) {
        let responseData;
        responseData = {
            "messages": [
                {
                    "text":  "Sorry. It seems you are not a registered user. No worries, I can help you set up your account.",
                    "quick_replies": [
                        {
                            "title":"Set up account",
                            "block_names": ["Get user details"]
                        }
                    ]
                }
            ]
        }
            return  res.status(200).send(responseData)
    });
}

const GETALLBACKUPGOALS = (req, res) => {
    const options = {
        method: 'GET',
        uri: BASE_URL+'api/v1/bot/user/backupgoal',
        qs: {
            phone: req.query.phone
        },
        headers: {Authorization: ""},
        json: true
    };
    let currentNum = Number(req.query.goal_number) === 1 ? Number(req.query.goal_number) - 1 : Number(req.query.goal_number)


    rp(options)
        .then(function (parsedBody) {
            let nextNum = (parsedBody.data.length - (Number(currentNum)+10) < 1 ? 0 : (Number(currentNum)+10))


            let responseData;

            const img = "https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/LetterG.svg/1200px-LetterG.svg.png"

            if(parsedBody.data.length > 0) {
                if(parsedBody.data.length > 1){
                let quick = parsedBody.data.slice(Number(currentNum), Number(currentNum) + 4).map((item, ind) => {
                    let subtitle = item.frequency === "daily" ? ( "Daily at " + item.hour_of_day+ " hrs ") : 
                    item.frequency === "weekly" ? ("Every " + item.day_of_week+ " day of the week at "+ item.hour_of_day+ " hrs ") :
                    item.frequency === "monthly" ? ("Every " + item.day_of_month+ " day of the month at "+ item.hour_of_day+ " hrs ") : ""
                    return (
                        {
                            "title":item.title +" "+ (ind+1),
                            "image_url":img,
                            "subtitle":subtitle,
                            "buttons":[
                                {
                                    "type": "show_block",
                                    "set_attributes":
                                        {
                                            "goal_id": item.id,
                                            "user_goal": currencyformatterJs.format((Number(item.target_amount)), {currency: 'NGN'}),
                                            "user_goal_title": item.title +" "+ (ind+1),
                                            "goal_subtitle": subtitle,
                                            "user_goal_start": currencyformatterJs.format((Number(item.start_amount)), {currency: 'NGN'}),
                                            "user_goal_start_date": item.start_date,
                                            "user_goal_end_date": item.end_date,

                                        },
                                    "block_names": ["Show goal details"],
                                    "title": "Show Details"
                                }
                            ]
                          }
                    )
                })
                // if(nextNum > 0){
                //     quick.push({
                //         "title": "Get more",
                //         "block_names": ["Add more steady save"]
                //     })
                // }

                responseData = {
                    "set_attributes":
                        {
                            "goal_number": nextNum
                        },
                    "messages": [
                        // {
                        //     "text": "...",
                        //     "quick_replies": quick


                        // }
                        {
                            "attachment":{
                              "type":"template",
                              "payload":{
                                "template_type":"list",
                                "top_element_style":"compact",
                                "elements": quick
                              }
                            }
                          }
                    ]
                }
            }else{
                let quick = parsedBody.data.slice(Number(currentNum), Number(currentNum) + 4).map((item, ind) => {
                    let subtitle = item.frequency === "daily" ? ( "Daily at " + item.hour_of_day+ " hrs ") : 
                    item.frequency === "weekly" ? ("Every " + item.day_of_week+ " day of the week at "+ item.hour_of_day+ " hrs ") :
                    item.frequency === "monthly" ? ("Every " + item.day_of_month+ " day of the month at "+ item.hour_of_day+ " hrs ") : ""
                    return (
                        {
                            "title":item.title +" "+ (ind+1),
                            "image_url":img,
                            "subtitle":subtitle,
                            "buttons":[
                                {
                                    "type": "show_block",
                                    "set_attributes":
                                        {
                                            "goal_id": item.id,
                                            "user_goal": currencyformatterJs.format((Number(item.target_amount)), {currency: 'NGN'}),
                                            "user_goal_title": item.title +" "+ (ind+1),
                                            "goal_subtitle": subtitle,
                                            "user_goal_start": currencyformatterJs.format((Number(item.start_amount)), {currency: 'NGN'}),
                                            "user_goal_start_date": item.start_date,
                                            "user_goal_end_date": item.end_date,

                                        },
                                    "block_names": ["Show goal details"],
                                    "title": "Show Details"
                                }
                            ]
                          }
                    )
                })
                // if(nextNum > 0){
                //     quick.push({
                //         "title": "Get more",
                //         "block_names": ["Add more steady save"]
                //     })
                // }

                responseData = {
                    "set_attributes":
                        {
                            "goal_number": nextNum
                        },
                    "messages": [
                        // {
                        //     "text": "...",
                        //     "quick_replies": quick


                        // }
                        {
                            "attachment":{
                                "type":"template",
                                "payload":{
                                  "template_type":"generic",
                                  "image_aspect_ratio": "square",
                                "elements": quick
                              }
                            }
                          }
                    ]
                }
            }
            }else{
                responseData = {
                    "messages": [
                        {
                            "text":  "You do not have any backup goal yet.",
                        }
                    ]
                }
            }

            res.status(201).send(responseData)
        }).catch(function (err) {
        let responseData;
        responseData = {
            "messages": [
                {
                    "text":  "Sorry. It seems you are not a registered user. No worries, I can help you set up your account.",
                    "quick_replies": [
                        {
                            "title":"Set up account",
                            "block_names": ["Get user details"]
                        }
                    ]
                }
            ]
        }
            return  res.status(200).send(responseData)
    });
}

const GETALLSTEADYDETAILS = (req, res) => {
    const options = {
        method: 'GET',
        uri: BASE_URL+'api/v1/bot/user/steadysave/'+req.query.id,
        qs: {
            
        },
        headers: {Authorization: ""},
        json: true
    };
    let currentNum = Number(req.query.sDetail_number) === 1 ? Number(req.query.sDetail_number) - 1 : Number(req.query.sDetail_number)


    rp(options)
        .then(function (parsedBody) {
            // let nextNum = (parsedBody.data.length - (Number(currentNum)+10) < 1 ? 0 : (Number(currentNum)+10))


            // let responseData;

            // const img = "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e4/Letter_s.svg/1200px-Letter_s.svg.png"

            // if(parsedBody.data.length > 0) {

            //     let quick = parsedBody.data.slice(Number(currentNum), Number(currentNum) + 4).map((item, ind) => {
            //         let subtitle = item.frequency === "daily" ? ( "Daily at " + item.hour_of_day+ " hrs ") : 
            //         item.frequency === "weekly" ? ("Every " + item.day_of_week+ " day of the week at "+ item.hour_of_day+ " hrs ") :
            //         item.frequency === "monthly" ? ("Every " + item.day_of_month+ " day of the month at "+ item.hour_of_day+ " hrs ") : ""
            //         return (
            //             {
            //                 "title":item.title +" "+ (ind+1),
            //                 "image_url":img,
            //                 "subtitle":subtitle,
            //                 "buttons":[
            //                     {
            //                         "type": "show_block",
            //                         "set_attributes":
            //                             {
            //                                 "steady_save_id": item.id
            //                             },
            //                         "block_names": ["Show steady details"],
            //                         "title": "Show History"
            //                     }
            //                 ]
            //               }
            //         )
            //     })
            //     // if(nextNum > 0){
            //     //     quick.push({
            //     //         "title": "Get more",
            //     //         "block_names": ["Add more steady save"]
            //     //     })
            //     // }

            //     responseData = {
            //         "set_attributes":
            //             {
            //                 "sDetail_number": nextNum
            //             },
            //         "messages": [
            //             // {
            //             //     "text": "...",
            //             //     "quick_replies": quick


            //             // }
            //             {
            //                 "attachment":{
            //                   "type":"template",
            //                   "payload":{
            //                     "template_type":"list",
            //                     "top_element_style":"compact",
            //                     "elements": quick
            //                   }
            //                 }
            //               }
            //         ]
            //     }
            // }else{
            //     responseData = {
            //         "messages": [
            //             {
            //                 "text":  "I'm currently unable to retrieve steady save. Please, try again later.",
            //             }
            //         ]
            //     }
            // }

            res.status(201).send(parsedBody)
        }).catch(function (err) {
        let responseData;
        responseData = {
                "messages": [
                    {
                        "text":  "I'm currently unable to complete the transaction. Please, try again later.",
                    }
                ]
            }
            return  res.status(200).send(responseData)
    });
}



const GETTENURE = (req, res) => {
    try{
    let start_date = req.query.locked_end_date
    const month = start_date.split("-")
    const dMon = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
    const days = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12,13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28,29,30,31]
    console.log("diff", (new Date(start_date) - new Date())/(3600*24*1000))
    if(month.length !== 3 || month[0].length !== 4 || !dMon.includes(Number(month[1])) || !days.includes(Number(month[2])) || (new Date(start_date) < new Date()) || 
    (((new Date(start_date) - new Date())/(3600*24*1000)) > 366)){
        
        let responseData = {
            "redirect_to_blocks": ["Wrong date format"]
        }
      return res.status(201).send(responseData)
    } else{
        const firstDate = new Date(start_date)
        const secondDate = new Date()
        let tenure = 0;
        var timeDiff = Math.abs(firstDate.getTime() - secondDate.getTime());

         // days difference
         tenure = Math.ceil(timeDiff / (1000 * 3600 * 24));
        let responseData;

        if(tenure < 30){
            responseData = {
                "redirect_to_blocks": ["Incorrect tenure"]
            }

        }else {
            responseData = {
                "set_attributes":
                    {
                        "locked_tenure": tenure,
                        "formatted_locked_amount": currencyformatterJs.format((Number(req.query.locked_amount)), {currency: 'NGN'})

                    },
                "redirect_to_blocks": ["Get locked interest"]
            }
        }
        return  res.status(200).send(responseData)
    }

}catch(err){
    let responseData;

    responseData = {
        "messages": [
            {
                "text":  "I'm currently unable to get tenure. Please, try again later.",
            }
        ]
    }
    return  res.status(200).send(responseData)
}
}


const CREATELOCKEDSAVINGS = (req, res) => {
    const options = {
        method: 'POST',
        uri: BASE_URL+'api/v1/bot/user/locked',
        body: {
            phone: req.body.user_phone_number,
            amount : req.body.locked_amount,
            tenure: req.body.locked_tenure
        },
        json: true
    };

    rp(options)
        .then(function (parsedBody) {
            /*let nextNum = (parsedBody.length - (Number(currentNum) + 10) < 1 ? 0 : (Number(currentNum) + 10))*/

            let amount = 0

            let responseData;
            
            responseData = {
                     "redirect_to_blocks": ["Locked success"]
                
            }
        // }
            res.status(201).send(responseData)
        }).catch(function (err) {
        let responseData;

        if(err.error && err.error.hasOwnProperty("message")){
            if(err.error.message.includes("Insufficient Amount")){
                responseData = {
                    "redirect_to_blocks": ["Locked failure"]
                }
            }
        }else {

            responseData = {
                "messages": [
                    {
                        "text": "I'm currently unable create this savings. Please, try again later.",
                    }
                ]
            }
        }
            return  res.status(200).send(responseData)
        /*}*/
    });
}

const GETALLLOCKEDSAVINGS = (req, res) => {
    const options = {
        method: 'POST',
        uri: BASE_URL+'api/v1/bot/user/locked/all',
        body: {
            phone: req.query.phone
        },
        headers: {Authorization: ""},
        json: true
    };
    let currentNum = Number(req.query.lock_number) === 1 ? Number(req.query.lock_number) - 1 : Number(req.query.lock_number)

/*{
            "id": 30,
            "user_id": "897332b0-978f-11e9-b1b7-e7bce63534fa",
            "amount": "30000",
            "title": "BOT_LOCKEDSAVE",
            "interest": "501.3698630136986",
            "start_date": "2019-08-01",
            "end_date": "2019-08-31",
            "account_id": 182,
            "created_at": "2019-08-01 07:26:52",
            "updated_at": "2019-08-01 07:26:52",
            "source": "central_vault"
        }*/
    rp(options)
        .then(function (parsedBody) {
            let nextNum = (parsedBody.data.length - (Number(currentNum)+10) < 1 ? 0 : (Number(currentNum)+10))


            let responseData;

            const img = "https://images-na.ssl-images-amazon.com/images/I/21CqefowmYL.jpg"

            if(parsedBody.data.length > 0) {
                if(parsedBody.data.length > 1){
                    let quick = parsedBody.data.slice(Number(currentNum), Number(currentNum) + 4).map((item, ind) => {
                        let subtitle = `From ${item.start_date} till ${item.end_date}`
                        return (
                            {
                                "title":item.title +" "+ (ind+1),
                                "image_url":img,
                                "subtitle":subtitle,
                                "buttons":[
                                    {
                                        "type": "show_block",
                                        "set_attributes":
                                            {
                                                "lock_id": item.id,
                                                "user_lock_title": item.title+" "+ (ind+1),
                                                "lock_subtitle": subtitle,
                                                "user_lock_amount": currencyformatterJs.format((Number(item.amount)), {currency: 'NGN'}),
                                                "user_lock_start_date": item.start_date,
                                                "user_lock_end_date": item.end_date,
                                                "user_lock_source": item.source === "central_vault" ? "Central Vault" : "Backup Stash",
                                                "lock_interest": currencyformatterJs.format((Number(item.interest)), {currency: 'NGN'})

                                            },
                                        "block_names": ["Show lock details"],
                                        "title": "Show Details"
                                    }
                                ]
                            }
                        )
                    })


                    responseData = {
                        "set_attributes":
                            {
                                "lock_number": nextNum
                            },
                        "messages": [
                            {
                                "attachment":{
                                    "type":"template",
                                    "payload":{
                                        "template_type":"list",
                                        "top_element_style":"compact",
                                        "elements": quick
                                    }
                                }
                            }
                        ]
                    }
                }else{
                    let quick = parsedBody.data.slice(Number(currentNum), Number(currentNum) + 4).map((item, ind) => {
                        let subtitle = `From ${item.start_date} till ${item.end_date}`
                         return (
                            {
                                "title":item.title +" "+ (ind+1),
                                "image_url":img,
                                "subtitle":subtitle,
                                "buttons":[
                                    {
                                        "type": "show_block",
                                        "set_attributes":
                                            {
                                                "lock_id": item.id,
                                                "user_lock_title": item.title+" "+ (ind+1),
                                                "lock_subtitle": subtitle,
                                                "user_lock_amount": currencyformatterJs.format((Number(item.amount)), {currency: 'NGN'}),
                                                "user_lock_start_date": item.start_date,
                                                "user_lock_end_date": item.end_date,
                                                "user_lock_source": item.source === "central_vault" ? "Central Vault" : "Backup Stash",
                                                "lock_interest": currencyformatterJs.format((Number(item.interest)), {currency: 'NGN'})

                                            },
                                        "block_names": ["Show lock details"],
                                        "title": "Show Details"
                                    }
                                ]
                            }
                        )
                    })


                    responseData = {
                        "set_attributes":
                            {
                                "lock_number": nextNum
                            },
                        "messages": [

                            {
                                "attachment":{
                                    "type":"template",
                                    "payload":{
                                        "template_type":"generic",
                                        "image_aspect_ratio": "square",
                                        "elements": quick
                                    }
                                }
                            }
                        ]
                    }
                }
            }else{
                responseData = {
                    "messages": [
                        {
                            "text":  "You do not have any locked savings yet.",
                        }
                    ]
                }
            }

            res.status(201).send(responseData)
        }).catch(function (err) {
        let responseData;
        responseData = {
            "messages": [
                {
                    "text":  "Sorry. It seems you are not a registered user. No worries, I can help you set up your account.",
                    "quick_replies": [
                        {
                            "title":"Set up account",
                            "block_names": ["Get user details"]
                        }
                    ]
                }
            ]
        }
        console.log(err)
        return  res.status(200).send(err)
    });
}

const GETGOALAMOUNT = (req, res) => {
    try{
        let start_date = req.query.goal_start_date
        let end_date = req.query.goal_end_date
        const month = start_date.split("-")
        const end = end_date.split("-")
        const dMon = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
        const days = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12,13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28,29,30,31]
        if(month.length !== 3 || month[0].length !== 4 || !dMon.includes(Number(month[1])) || !days.includes(Number(month[2])) || 
       end.length !== 3 || end[0].length !== 4 || !dMon.includes(Number(end[1])) || !days.includes(Number(end[2])) ||
       (new Date(start_date) > new Date(end_date)) || 
        (new Date(start_date) < new Date())){
            let responseData = {
                "redirect_to_blocks": ["Re-enter start date Goal"]
            }
           return res.status(201).send(responseData)
        } else{
        const firstDate = new Date(start_date)
        const secondDate = new Date(end_date)
        let tenure = 0;
        var timeDiff = Math.abs(secondDate.getTime() - firstDate.getTime());

         // days difference
         tenure = Math.floor(timeDiff / (1000 * 3600 * 24));
         let frequency = req.query.frequency
         let contribution = 0
         let goal = req.query.backup_goal
         let timeMeter;
         if(frequency === "Daily"){
            timeMeter = tenure
             contribution = Math.ceil(goal/timeMeter)
         }else if(frequency === "Weekly"){
            timeMeter = Math.floor(tenure / 7)
            contribution = Math.ceil(goal/timeMeter)
         }else{
            timeMeter = Math.floor(tenure / (7 * 4))
            contribution = Math.ceil(goal/timeMeter)
         }
        let responseData;
        console.log(timeDiff, timeMeter, contribution)

            responseData = {
                "set_attributes":
                    {
                        "backup_amount": contribution,
                        "formatted_backup_amount" : currencyformatterJs.format((Number(contribution)), {currency: 'NGN'}),
                        "formatted_backup_goal" : currencyformatterJs.format((Number(goal)), {currency: 'NGN'})
                        
                    },
                    "redirect_to_blocks": ["Get Goal bank"]
               }

        return  res.status(200).send(responseData)
    }

}catch(err){
    let responseData;

    responseData = {
        "messages": [
            {
                "text":  "I'm currently unable to get tenure. Please, try again later.",
            }
        ]
    }
    return  res.status(200).send(responseData)
}
}

const AUTHORIZECARD = (req, res) => {
    const options = {
        method: 'POST',
        uri: BASE_URL+'api/v1/bot/user/transaction/init',
        body: {
            amount: 100,
            source: "quick",
            phone: req.body.user_phone_number,
        },
        json: true
    };
    let name = req.body.first_name

    rp(options)
        .then(function (parsedBody) {
            /*let nextNum = (parsedBody.length - (Number(currentNum) + 10) < 1 ? 0 : (Number(currentNum) + 10))*/

            let response = JSON.parse(JSON.stringify(parsedBody))
            let amount =100
            let reference = response.data.reference
            let messengerId = req.body.messenger_id
            let phone = req.body.user_phone_number
            let cusEmail = req.body.user_email
            let responseData = {
                "set_attributes":
                    {
                        "reference": response.data.reference,
                        "account_balance": currencyformatterJs.format((Number(response.account_balance) + Number(amount)), {currency: 'NGN'}),
                        "formatted_amount": currencyformatterJs.format((Number(amount)), {currency: 'NGN'})
                    },
                "messages": [
                    {
                        "attachment": {
                            "type": "template",
                            "payload": {
                                "template_type": "button",
                                "text": "In order to add your card, a sum of "+currencyformatterJs.format((Number(amount)), {currency: 'NGN'})+ " will be charged. This will be added to your central vault after a successful charge. Do you wish to proceed?",
                                "buttons": [
                                    {
                                        "type": "web_url",
                                        "url": "https://backupcash-bot.herokuapp.com/authorize?amount="+amount+"&reference="+reference+"&email="+cusEmail+"&messengerId="+messengerId+"&phone="+phone,
                                        "title": "Add Card"
                                    },
                                    {
                                        "type": "show_block",
                                        "block_names": ["Cancel Operation"],
                                        "title": "Cancel"
                                    }
                                ]
                            }
                        }
                    }
                ]
            }
            res.status(201).send(responseData)
        }).catch(function (err) {
        console.log("error",err)
        let responseData;
        if(JSON.parse(JSON.stringify(err.error.message))){
            responseData = {
                "messages": [
                    {
                        "text":  "Sorry, "+name+ ". It seems you don't have an account yet. But I can help you set it up.",
                        "quick_replies": [
                            {
                                "title":"Set up account",
                                "block_names": ["Get user details"]
                            }
                        ]
                    }
                ]
            }
            return  res.status(200).send(responseData)
        }
        else {
            responseData = {
                "messages": [
                    {
                        "text":  "I'm currently unable to complete the transaction. Please, try again later.",
                    }
                ]
            }
            return  res.status(200).send(responseData)
        }
    });
}

const GETSTEADYSAVEMENU = (req, res) => {
    const options = {
        method: 'GET',
        uri: BASE_URL+'api/v1/bot/user/steadysave',
        qs: {
            phone: req.query.phone
        },
        headers: {Authorization: ""},
        json: true
    };
   
    rp(options)
        .then(function (parsedBody) {
            

            let responseData;

            if(parsedBody.data.length > 0) {
                
                responseData = {
                    "redirect_to_blocks": ["Menu 2"]
                }
            }else{
                responseData = {
                    "redirect_to_blocks": ["Menus"]
                }
            }

            res.status(201).send(responseData)
        }).catch(function (err) {
        let responseData;
        console.log("menu", err)
        if(JSON.parse(JSON.stringify(err.error.message)).includes("User not found")){
            responseData = {
                "messages": [
                    {
                        "text":  "Sorry. It seems you are not a registered user. No worries, I can help you set up your account.",
                        "quick_replies": [
                            {
                                "title":"Set up account",
                                "block_names": ["Get user details"]
                            }
                        ]
                    }
                ]
            }
            return  res.status(200).send(responseData)
        }
        else {
            responseData = {
                "messages": [
                    {
                        "text":  "Connection failed.",
                        
                    }
                ]
            }
                return  res.status(200).send(responseData)
        }
        
    });
}

const GETUSERWITDRAWALBALANCE = (req, res) => {
    const options = {
        method: 'POST',
        uri: BASE_URL+'api/v1/bot/user/info',
        body: {
            phone: req.query.user_phone_number,
        },
        json: true
    };

    // "central_vault": 0,
    // "backup_stash": 0,
    // "interest": 0,
    // "locked": 0,
    // "backup_goal": 0,
   

    rp(options)
        .then(function (parsedBody) {


           let responseData;

            responseData = {
                "set_attributes":
                    {
                        "central_vault": currencyformatterJs.format((Number(parsedBody.central_vault)), {currency: 'NGN'}),
                        "backup_stash": currencyformatterJs.format((Number(parsedBody.backup_stash)), {currency: 'NGN'}),
                        "locked_savings": currencyformatterJs.format((Number(parsedBody.locked)), {currency: 'NGN'}),
                        "interest": currencyformatterJs.format((Number(parsedBody.interest)), {currency: 'NGN'}),
                        "backup_savings": currencyformatterJs.format((Number(parsedBody.backup_goal)), {currency: 'NGN'}),
                        "user_phone_number": parsedBody.user.phone,
                        "user_email": parsedBody.user.email,
                        "referral_code": parsedBody.user.referral_code,
                        "referral_link": parsedBody.user.referral_link,
                        "user_first_name": parsedBody.user.name,
                        "user_last_name": parsedBody.user.last_name
                    },
                    "redirect_to_blocks": ["Savings Balance"]
               }
            res.status(201).send(responseData)
        }).catch(function (err) {
        console.log("error",err)
        let responseData;
        if(JSON.parse(JSON.stringify(err.error.message))){
            responseData = {
                "messages": [
                    {
                        "text":  "Sorry, "+req.query.name+ ". It seems you are not a registered user. No worries, I can help you set up your account.",
                        "quick_replies": [
                            {
                                "title":"Set up account",
                                "block_names": ["Get user details"]
                            }
                        ]
                    }
                ]
            }
            return  res.status(200).send(responseData)
        }
        else {
            responseData = {
                "messages": [
                    {
                        "text":  "I'm currently unable to complete the transaction. Please, try again later.",
                    }
                ]
            }
            return  res.status(200).send(responseData)
        }
    });
}

const DEFAULTRESPONSE = (req, res) =>{
     try {


         const accountOpening = ["create my own account", "create account", "interested", "I'm interested", "Am interested", "help me", "create account",
             "sign up", "set it up", "setup", "set up", "how can I be able to sign up", "how can I be able to register", "I want to join", " Get Started", "I have not registered", "how do I start", "set up my account",
             "be part", "what to do", "I want to join", "the next step", "Am ready", "create my own account"]
         const instantSave = ["instant save"]
         const steadySave = ["21 day savings challenge"]
         const lockedSavings = ["locked saving"]
         const backupGoals = ["backup goal"]
         const greetings = ["Good Morning", "Good afternoon", "Good Evening"]
         const withdrawal = ["withdrawal", "i want to withdraw"]
         const product = ["learn more", "how does it", "how do I", "i want to know what", "i want to know how it", "how does it work", "Explain", "how this works",
             "want tot know", "all about", "tell me more", "know more about it", "start saving money", "what do you understand", "I want to save"]

         const rules = [{name: "accountOpening", value: accountOpening, block: "Get user details"},
             {name: "instantSave", value: instantSave, block: "Initiate deposit"},
             {name: "steadySave", value: steadySave, block: "Get steady amount"},
             {name: "lockedSavings", value: lockedSavings, block: "Get locked details"},
             {name: "backupGoals", value: backupGoals, block: "Get goal amount"},
             {name: "withdrawal", value: withdrawal, block: "Verify User"},
             {name: "product", value: product, block: "Product summary"},
             {name: "greetings", value: greetings, block: "Welcome Message"}]
         let matchingRatio = rules.map((rule) => {
             return (
                 {
                     name: rule.name,
                     block: rule.block,
                     ratings: stringSimilarity.findBestMatch(req.body.sentence, rule.value).bestMatch
                 }
             )
         })
         let sorted = matchingRatio.sort((a, b) => (b.ratings.rating - a.ratings.rating))

         console.log(sorted)

         let responseData;

         responseData = {
             "redirect_to_blocks": [sorted[0].block]
         }

         return res.status(200).send(responseData)
     }catch (e) {
         let responseData;

         responseData = {
             "redirect_to_blocks": ["Product Details"]
         }

         return res.status(200).send(responseData)
     }
}




module.exports = {
    CREATE, DEPOSIT, LOG, USERBANK, GETBANKS, ADDACCOUNT, GETWITHDRAWALDATE, ADDUSERWITHDRAWALDATE, GETPENALTY, MAKEWITHDRAWAL,
    GETUSERDETAILS, USERBANKSTEADY, CREATESTEADYSAVE, CREATEBACKUPGOAL, GETALLSTEADYSAVE, GETALLSTEADYDETAILS, 
    GETALLBACKUPGOALS, GETTENURE, CREATELOCKEDSAVINGS, GETGOALAMOUNT, USERCARD, INSTANTSAVE, AUTHORIZECARD, SETWITHDRAWALPIN,
    GETLOCKEDINTEREST, GETSTEADYSAVEMENU, GETUSERWITDRAWALBALANCE, GETALLLOCKEDSAVINGS, DEFAULTRESPONSE
}