'use strict';
const express = require('express');
const router = express.Router()
const {CREATE, DEPOSIT, LOG, USERBANK, GETBANKS, ADDACCOUNT, GETWITHDRAWALDATE, ADDUSERWITHDRAWALDATE, GETPENALTY,
    GETUSERDETAILS,MAKEWITHDRAWAL, USERBANKSTEADY, CREATESTEADYSAVE, CREATEBACKUPGOAL, GETALLSTEADYSAVE, GETALLSTEADYDETAILS,
GETALLBACKUPGOALS, GETTENURE, CREATELOCKEDSAVINGS, GETGOALAMOUNT, USERCARD, INSTANTSAVE, AUTHORIZECARD, SETWITHDRAWALPIN,
GETLOCKEDINTEREST, GETSTEADYSAVEMENU, GETUSERWITDRAWALBALANCE, GETALLLOCKEDSAVINGS, DEFAULTRESPONSE}= require('../controllers/account_creation');


router.use('/create-account',  CREATE);
router.use('/deposit',  DEPOSIT);
router.use('/instant-save',  INSTANTSAVE)
router.use('/authorize',  AUTHORIZECARD)
router.use('/log',  LOG);
router.use('/bank',  USERBANK);
router.use('/card',  USERCARD);
router.use('/staedy-bank',  USERBANKSTEADY);
router.use('/all-bank',  GETBANKS);
router.use('/setup',  ADDACCOUNT);
router.use('/get-withdrawal-date',  GETWITHDRAWALDATE);
router.use('/set-withdrawal-date',  ADDUSERWITHDRAWALDATE);
router.use('/get-penalty',  GETPENALTY);
router.use('/withdraw',  MAKEWITHDRAWAL);
router.use('/get-user',  GETUSERDETAILS);
router.use('/steady-save',  CREATESTEADYSAVE);
router.use('/get-all-steady',  GETALLSTEADYSAVE);
router.use('/get-steady-details',  GETALLSTEADYDETAILS);
router.use('/backup-goal',  CREATEBACKUPGOAL);
router.use('/get-all-goals',  GETALLBACKUPGOALS);
router.use('/get-tenure',  GETTENURE);
router.use('/locked-savings',  CREATELOCKEDSAVINGS);
router.use('/goal-amount',  GETGOALAMOUNT);
router.use('/set-pin',  SETWITHDRAWALPIN);
router.use('/get-interest',  GETLOCKEDINTEREST);
router.use('/menu-steady',  GETSTEADYSAVEMENU);
router.use('/savings-balance',  GETUSERWITDRAWALBALANCE);
router.use('/get-all-locked',  GETALLLOCKEDSAVINGS);
router.use('/match-words',  DEFAULTRESPONSE);

module.exports = router;