const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const cors = require('cors')
//require("dotenv").config();
//require('dotenv').config({path: __dirname + '/hydrate.env'});
require("express-async-errors");

/*
const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');*/


const app = express();


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors())


require('./routes')(app);
app.use('/', (req, res)=>{
  res.status(200).json("Hallo")
});
/*app.use('/users', usersRouter);*/

app.use(function (req, res, next) {

  console.log(req.headers.origin)
  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', '*');

  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

  // Request headers you wish to allow
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,authorization,Authorization,token');

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader('Access-Control-Allow-Credentials', true);

  // Pass to next layer of middleware
  next();
});




// catch 404 and forward to error handler
app.use(function(req, res, next) {
  // console.error("Error", JSON.stringify())
  next(createError(404));
});



// error handler
// app.use(function(err, req, res, next) {
//   // set locals, only providing error in development
//   res.locals.message = err.message;
//   res.locals.error = req.app.get('env') === 'development' ? err : {};
//
//   slack.send({
//     text: `${err.message} --------------- ${JSON.stringify(err)}`
//   });
//   console.error("--------------------------------------------------------------");
//   console.error(err.message, JSON.stringify(err));
//   // render the error page
//   // res.status(err.status || 500);
//   res.render('error');
//   res.status(400).send(err)
//
//
// });

module.exports = app;
